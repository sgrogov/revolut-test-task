import React from 'react';
import PropTypes from 'prop-types';
import './exchange-control.css';
import {CURRENCY, CURRENCY_SYMBOL} from '../../../../constants';
import CurrencyInput from '../CurrencyInput';

class ExchangeControl extends React.PureComponent {
    static propTypes = {
        buyingCurrency: PropTypes.oneOf(Object.values(CURRENCY)).isRequired,
        sellingCurrency: PropTypes.oneOf(Object.values(CURRENCY)),
        value: PropTypes.string,
        balance: PropTypes.number,
        rate: PropTypes.number,
        sign: PropTypes.oneOf(['-', '+']),
        readOnly: PropTypes.bool,
        onFocus: PropTypes.func,
        onChange: PropTypes.func,
    }

    renderBalanceMessage() {
        const {balance, buyingCurrency} = this.props;
        if (balance === undefined) return '';

        const formattedBalance = Math.floor(balance * 100) / 100;

        return `You have ${CURRENCY_SYMBOL[buyingCurrency]}${formattedBalance}`;
    }

    renderRateMessage() {
        const {rate, buyingCurrency, sellingCurrency} = this.props;
        if (rate === undefined || sellingCurrency === undefined) return '';

        const buyingSymbol = CURRENCY_SYMBOL[buyingCurrency];
        const buyingAmount = 1;
        const sellingSymbol = CURRENCY_SYMBOL[sellingCurrency];
        const sellingAmount = rate.toFixed(2);

        return `${buyingSymbol}${buyingAmount} = ${sellingSymbol}${sellingAmount}`;
    }

    render() {
        const {balance, buyingCurrency, value, readOnly, onFocus, onChange} = this.props;

        return (
            <div className="exchange-control">
                <div className="exchange-control__row">
                    <span className="exchange-control__currency">{buyingCurrency}</span>
                    <CurrencyInput
                        className="exchange-control__value"
                        value={value}
                        readOnly={readOnly}
                        onFocus={onFocus}
                        onChange={onChange}
                    />
                </div>
                <div className="exchange-control__row">
                    <span
                        key={balance}
                        className="exchange-control__balance"
                    >{this.renderBalanceMessage()}</span>
                    <span className="exchange-control__rate">{this.renderRateMessage()}</span>
                </div>
            </div>
        )
    }
}

export default ExchangeControl;
