import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './currency-input.css';

class CurrencyInput extends React.Component {
    static propTypes = {
        className: PropTypes.string,
        value: PropTypes.string,
        readOnly: PropTypes.bool,
        onFocus: PropTypes.func,
        onChange: PropTypes.func,
    }

    constructor(props) {
        super(props);

        this.state = {
            value: props.value,
        };
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.value !== nextProps.value) {
            this.setState({
                value: nextProps.value,
            });
        }
    }

    onChange = (e) => {
        const prevStateValue = this.state.value;

        let value = e.target.value;
        if (e.target.value === '.') {
            value = '0.';
        } else if (['0', '-', '+'].includes(e.target.value)) {
            value = '';
        } else {
            if (isNaN(e.target.value)) return;

            const [, fraction] = e.target.value.split('.');
            if (typeof fraction === 'string' && fraction.length > 2) return;
        }

        this.setState({value}, () => {
            if (typeof this.props.onChange === 'function' && Number(this.state.value) !== Number(prevStateValue)) {
                this.props.onChange(this.state.value);
            }
        });
    }

    render() {
        return (
            <div className={cn('currency-input', this.props.className)}>
                <input
                    type="text"
                    value={this.state.value}
                    readOnly={this.props.readOnly}
                    onFocus={this.props.onFocus}
                    onChange={this.onChange}
                />
            </div>
        );
    }
}

export default CurrencyInput;
