import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Screen from '../Screen';
import './exchange-screen.css';
import Button from '../../components/Button';
import Slider from '../../components/Slider';
import ExchangeControl from './components/ExchangeControl';
import {CURRENCY} from '../../constants';
import {
    selectSellingCurrency,
    selectBuyingCurrency,
    selectExchangeRate,
    selectSellingAmount,
    selectBuyingAmount,
    setBuyingCurrency,
    setSellingCurrency,
    setSellingAmount, startListeningToRateChanges, stopListeningToRateChanges, commitExchange,
} from './reducer';
import {fetchBalance} from '../Account/reducer';

class ExchangeScreen extends React.Component {
    static propTypes = {
        currencies: PropTypes.arrayOf(PropTypes.oneOf(Object.values(CURRENCY))),
        sellingCurrency: PropTypes.oneOf(Object.values(CURRENCY)),
        buyingCurrency: PropTypes.oneOf(Object.values(CURRENCY)),
        sellingAmount: PropTypes.number,
        buyingAmount: PropTypes.number,
        rate: PropTypes.number,
        balance: PropTypes.object,
        onMount: PropTypes.func.isRequired,
        onUnmount: PropTypes.func.isRequired,
        onSellingCurrencyChange: PropTypes.func.isRequired,
        onBuyingCurrencyChange: PropTypes.func.isRequired,
        onSellingAmountChange: PropTypes.func.isRequired,
        onExchangeCommit: PropTypes.func.isRequired,
    }

    componentDidMount() {
        this.props.onMount();
    }
    componentWillUnmount() {
        this.props.onUnmount();
    }

    onSellingCurrencyChange = (currencyIndex) => {
        const currency = this.props.currencies[currencyIndex];
        this.props.onSellingCurrencyChange(currency);
    }
    onBuyingCurrencyChange = (currencyIndex) => {
        const currency = this.props.currencies[currencyIndex];
        this.props.onBuyingCurrencyChange(currency);
    }
    onSellingAmountChange = (value) => {
        let amount = Math.abs(Number(value));

        if (isNaN(amount) || amount === 0) {
            amount = null;
        }

        this.props.onSellingAmountChange(amount);
    }
    onExchangeCommit = () => {
        this.props.onExchangeCommit(
            [this.props.sellingCurrency, this.props.buyingCurrency].join(''),
            this.props.sellingAmount,
        );
    }

    renderLeftButton() {
        return (
            <Button
                text="Cancel"
                disabled
            />
        );
    }

    renderRightButton() {
        return (
            <Button
                text="Exchange"
                disabled={this.props.buyingAmount === null}
                onClick={this.onExchangeCommit}
            />
        );
    }

    renderSellingControls() {
        const value = this.props.sellingAmount === null
            ? ''
            : `-${this.props.sellingAmount}`;

        return this.props.currencies.map((currency, i) => (
            <ExchangeControl
                buyingCurrency={currency}
                balance={this.props.balance[currency]}
                value={value}
                onFocus={() => this.onSellingCurrencyChange(i)}
                onChange={this.onSellingAmountChange}
            />
        ));
    }
    formatBuyingAmount(amount) {
        let value = '';
        if (amount !== null) {
            value = `+${Math.floor(amount * 100) / 100}`;
        }
        return value;
    }
    renderBuyingControls() {
        return this.props.currencies.map((currency, i) => (
            <ExchangeControl
                buyingCurrency={currency}
                sellingCurrency={this.props.sellingCurrency}
                balance={this.props.balance[currency]}
                value={this.formatBuyingAmount(this.props.buyingAmount)}
                rate={this.props.rate}
                readOnly
                onFocus={() => this.onBuyingCurrencyChange(i)}
            />
        ));
    }

    render() {
        const {currencies, sellingCurrency, buyingCurrency} = this.props;

        const sellingCurrencyIndex = currencies.indexOf(sellingCurrency);
        const buyingCurrencyIndex = currencies.indexOf(buyingCurrency);

        return (
            <Screen
                id="exchange"
                leftButton={this.renderLeftButton()}
                rightButton={this.renderRightButton()}
            >
                <div className="exchange-screen__top">
                    <Slider
                        slides={this.renderSellingControls()}
                        slide={sellingCurrencyIndex}
                        onSlideChange={this.onSellingCurrencyChange}
                    />
                </div>
                <div className="exchange-screen__bottom">
                    <Slider
                        slides={this.renderBuyingControls()}
                        slide={buyingCurrencyIndex}
                        onSlideChange={this.onBuyingCurrencyChange}
                    />
                </div>
            </Screen>
        );
    }
}

const mapStateToProps = state => ({
    currencies: state.exchange.currencies,
    sellingCurrency: selectSellingCurrency(state),
    buyingCurrency: selectBuyingCurrency(state),
    sellingAmount: selectSellingAmount(state),
    buyingAmount: selectBuyingAmount(state),
    balance: state.account.balance,
    rate: selectExchangeRate(state),
});
const mapDispatchToProps = dispatch => ({
    onMount: () => {
        dispatch(fetchBalance());
        dispatch(startListeningToRateChanges());
    },
    onUnmount: () => dispatch(stopListeningToRateChanges()),
    onSellingCurrencyChange: currency => dispatch(setSellingCurrency(currency)),
    onBuyingCurrencyChange: currency => dispatch(setBuyingCurrency(currency)),
    onSellingAmountChange: amount => dispatch(setSellingAmount(amount)),
    onExchangeCommit: (pair, amount) => dispatch(commitExchange(pair, amount)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ExchangeScreen);
