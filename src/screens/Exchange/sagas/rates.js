import {delay} from 'redux-saga';
import {take, put, call, fork, cancel} from 'redux-saga/effects';
import Api from '../../../api';
import {
    fetchRatesSuccess,
    setListening,
    START_LISTENING_TO_RATE_CHANGES,
    STOP_LISTENING_TO_RATE_CHANGES,
} from '../reducer';
import {EXCHANGE_RATE_UPDATE_INTERVAL} from '../../../configuration';

function* synchronizeRates() {
    while (true) {
        const rates = yield call(Api.exchange.rates);
        yield put(fetchRatesSuccess(rates));

        yield delay(EXCHANGE_RATE_UPDATE_INTERVAL);
    }
}

export default function* watchExchangeRatesListening() {
    while (yield take(START_LISTENING_TO_RATE_CHANGES)) {
        yield put(setListening(true));

        const ratesSyncTask = yield fork(synchronizeRates);

        yield take(STOP_LISTENING_TO_RATE_CHANGES);
        yield put(setListening(false));

        yield cancel(ratesSyncTask);
    }
}
