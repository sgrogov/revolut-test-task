import {takeEvery, put, call} from 'redux-saga/effects';
import Api from '../../../api';
import {COMMIT_EXCHANGE} from '../reducer';
import {commitExchangeSuccess} from '../../Exchange/reducer';

function* commitExchange(action) {
    const {pair, amount} = action.payload;

    try {
        const balance = yield call(Api.exchange.commit, pair, amount);

        yield put(commitExchangeSuccess(balance));
    } catch (e) {
        alert(e);
    }
}

export default function* watchExchangeCommits() {
    yield takeEvery(COMMIT_EXCHANGE, commitExchange);
}
