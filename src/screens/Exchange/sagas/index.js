import {all} from 'redux-saga/effects';
import commit from './commit';
import rates from './rates';

export default function* exchangeRootSaga() {
    yield all([
        commit(),
        rates(),
    ]);
}
