import {CURRENCY} from '../../constants';

const action = (type, payload) => ({type, payload});

const {EUR, GBP, USD} = CURRENCY;

const initialState = {
    isListening: false,
    currencies: [EUR, GBP, USD],
    sellingCurrency: EUR,
    buyingCurrency: GBP,
    sellingAmount: null,
    rates: {},
};

export const selectSellingCurrency = state => state.exchange.sellingCurrency;
export const selectBuyingCurrency = state => state.exchange.buyingCurrency;

export const selectRate = (state, sellingCurrency, buyingCurrency) =>
    state.exchange.rates[`${sellingCurrency}${buyingCurrency}`];
export const selectExchangeRate = (state) => {
    const {sellingCurrency, buyingCurrency} = state.exchange;
    return selectRate(state, sellingCurrency, buyingCurrency);
};

export const selectSellingAmount = state => state.exchange.sellingAmount;
export const selectBuyingAmount = (state) => {
    const sellingAmount = selectSellingAmount(state);
    const rate = selectRate(
        state,
        selectSellingCurrency(state),
        selectBuyingCurrency(state),
    );

    if (rate === undefined || sellingAmount === null) return null;

    return sellingAmount * rate;
};

const SET_SELLING_CURRENCY = 'EXCHANGE/SET_SELLING_CURRENCY';
const SET_BUYING_CURRENCY = 'EXCHANGE/SET_BUYING_CURRENCY';
export const setSellingCurrency = currency => action(SET_SELLING_CURRENCY, {currency});
export const setBuyingCurrency = currency => action(SET_BUYING_CURRENCY, {currency});

const SET_SELLING_AMOUNT = 'EXCHANGE/SET_SELLING_AMOUNT';
export const setSellingAmount = amount => action(SET_SELLING_AMOUNT, {amount});

export const START_LISTENING_TO_RATE_CHANGES = 'EXCHANGE/START_LISTENING_TO_RATE_CHANGES';
export const STOP_LISTENING_TO_RATE_CHANGES = 'EXCHANGE/STOP_LISTENING_TO_RATE_CHANGES';
export const startListeningToRateChanges = () => action(START_LISTENING_TO_RATE_CHANGES);
export const stopListeningToRateChanges = () => action(STOP_LISTENING_TO_RATE_CHANGES);

export const SET_LISTENING = 'EXCHANGE/LISTENING';
export const setListening = isListening => action(SET_LISTENING, {isListening});

const FETCH_RATES_SUCCESS = 'EXCHANGE/FETCH_RATES_SUCCESS';
export const fetchRatesSuccess = rates => action(FETCH_RATES_SUCCESS, {rates});

export const COMMIT_EXCHANGE = 'EXCHANGE/COMMIT';
export const commitExchange = (pair, amount) => action(COMMIT_EXCHANGE, {pair, amount});
export const COMMIT_EXCHANGE_SUCCESS = 'EXCHANGE/COMMIT_SUCCESS';
export const commitExchangeSuccess = balance => action(COMMIT_EXCHANGE_SUCCESS, {balance});

const exchangeReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_SELLING_CURRENCY:
            return {
                ...state,
                sellingCurrency: action.payload.currency,
            };
        case SET_BUYING_CURRENCY:
            return {
                ...state,
                buyingCurrency: action.payload.currency,
            };
        case SET_SELLING_AMOUNT:
            return {
                ...state,
                sellingAmount: action.payload.amount,
            };
        case SET_LISTENING:
            return {
                ...state,
                isListening: action.payload.isListening,
            };
        case FETCH_RATES_SUCCESS:
            return {
                ...state,
                rates: action.payload.rates,
            };
        case COMMIT_EXCHANGE_SUCCESS:
            return {
                ...state,
                sellingAmount: null,
            };
        default:
            return state;
    }
};

export default exchangeReducer;
