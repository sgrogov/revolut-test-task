import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './screen.css';
import ScreenBackground from '../components/ScreenBackground';

class Screen extends React.Component {
    static propTypes = {
        id: PropTypes.string.isRequired,
        leftButton: PropTypes.node,
        rightButton: PropTypes.node,
        children: PropTypes.node
    }

    render() {
        const {id, leftButton, rightButton, children} = this.props;

        return (
            <div className={cn('screen', `screen__${id}`)}>
                <ScreenBackground
                    className="screen__background"
                    seed={id}
                />
                <div className="screen__head">
                    {leftButton}
                    {rightButton}
                </div>
                <div className={cn('screen__body', `${id}-screen`)}>
                    {children}
                </div>
            </div>
        );
    }
}

export default Screen;
