import {takeLatest, put, call} from 'redux-saga/effects';
import Api from '../../../api';
import {FETCH_BALANCE, fetchBalanceSuccess} from '../reducer';

function* fetchBalance() {
    const balance = yield call(Api.account.balance);
    yield put(fetchBalanceSuccess(balance));
}

export default function* watchBalanceRequests() {
    yield takeLatest(FETCH_BALANCE, fetchBalance);
}
