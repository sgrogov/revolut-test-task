import {all} from 'redux-saga/effects';
import balance from './balance';

export default function* accountRootSaga() {
    yield all([
        balance(),
    ]);
}
