import {COMMIT_EXCHANGE_SUCCESS} from '../Exchange/reducer';

const initialState = {
    balance: {},
};

export const FETCH_BALANCE = 'ACCOUNT/FETCH_BALANCE';
export const fetchBalance = () => ({type: FETCH_BALANCE});
const FETCH_BALANCE_SUCCESS = 'ACCOUNT/FETCH_BALANCE_SUCCESS';
export const fetchBalanceSuccess = balance => ({
    type: FETCH_BALANCE_SUCCESS,
    payload: {balance},
});

const accountReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_BALANCE_SUCCESS:
        case COMMIT_EXCHANGE_SUCCESS:
            return {
                ...state,
                balance: action.payload.balance,
            };
        default:
            return state;
    }
};

export default accountReducer;
