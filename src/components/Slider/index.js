import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import './slider.css';

class Slider extends React.PureComponent {
    static propTypes = {
        slides: PropTypes.arrayOf(PropTypes.node).isRequired,
        slide: PropTypes.number,
        onSlideChange: PropTypes.func.isRequired,
    }
    static defaultProps = {
        slide: 0,
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.slide !== nextProps.slide) {
            this.slider.scrollLeft = 0;
        }
    }

    onSlidesRef = (slides) => {
        this.slides = slides;

        if (slides !== null) {
            let isDragging = false;
            let initialPosition;
            let offset = 0;
            const onStart = (e) => {
                isDragging = true;
                initialPosition = e.screenX;
            };
            const onMove = (e) => {
                if (!isDragging) return;

                offset = e.screenX - initialPosition;
                this.slides.style.setProperty('--slider-drag-offset', offset);
            };
            const onStop = () => {
                if (!isDragging) return;

                isDragging = false;

                let slide = this.props.slide - Math.round(offset / this.slides.offsetWidth);
                slide = Math.max(0, Math.min(this.props.slides.length - 1, slide));
                offset = 0;

                this.slider.style.setProperty('--slider-slide-number', slide);
                this.slides.style.setProperty('--slider-drag-offset', offset);

                if (this.props.slide !== slide) {
                    this.props.onSlideChange(slide);
                }
            };
            this.slides.addEventListener('mousedown', onStart);
            document.addEventListener('mousemove', onMove);
            document.addEventListener('mouseup', onStop);
        }
    }

    render() {
        const {slides} = this.props;
        return (
            <div
                ref={slider => this.slider = slider}
                className="slider"
                style={{'--slider-slide-number': this.props.slide}}
            >
                <div
                    ref={this.onSlidesRef}
                    className="slider__slides"
                >
                    {slides.map((slide, i) => (
                        <div key={i} className="slider__slide">{slide}</div>
                    ))}
                </div>
                <div className="slider__dots">
                    {slides.map((slide, i) => (
                        <div
                            key={i}
                            className={cn('slider__dot', {
                                slider__dot_active: i === this.props.slide,
                            })}
                        />
                    ))}
                </div>
            </div>
        );
    }
}

export default Slider;
