import React from 'react';
import PropTypes from 'prop-types';

const MAX_DOT_SIZE = 0.15;
const MIN_DOT_SIZE = 0.01;
const MAX_DOT_OPACITY = 0.25;
const MIN_DOT_OPACITY = 0.05;

class ScreenBackground extends React.PureComponent {
    static propTypes = {
        className: PropTypes.string,
        seed: PropTypes.string,
    }
    static defaultProps = {
        seed: 'default',
    }

    state = {
        width: 0,
        height: 0,
    }

    wrapper = null
    canvas = null

    onWrapperRef = (wrapper) => {
        const dpi = window.devicePixelRatio;

        this.setState({
            width: wrapper.offsetWidth * dpi,
            height: wrapper.offsetHeight * dpi,
        });
    }
    onCanvasRef = canvas => this.canvas = canvas

    componentDidUpdate() {
        if (this.canvas !== null) {
            this.draw();
        }
    }

    produceDotsData() {
        const {seed} = this.props;
        const dotsAmount = 15 + (seed.length) * 100 % 30;

        const dots = [];
        for (let i = 0; i < dotsAmount; i++) {
            const x = (seed.charCodeAt(i % seed.length) + i) % 30 / 30;
            const y = (seed.charCodeAt((i + 1)% seed.length) + i) % 30 / 30;
            const z = ((seed.charCodeAt((i + 2)% seed.length) + i) % 30 / 30) ** 10;

            dots.push([x, y, z]);
        }

        return dots;
    }
    draw() {
        const dots = this.produceDotsData();

        const ctx = this.canvas.getContext('2d');
        const {width, height} = this.state;

        for (const dot of dots) {
            const [x, y, z] = dot;
            const radius = MIN_DOT_SIZE + (MAX_DOT_SIZE - MIN_DOT_SIZE) * z;
            const opacity = MIN_DOT_OPACITY + (MAX_DOT_OPACITY - MIN_DOT_OPACITY) * z;

            ctx.beginPath();
            ctx.arc(
                x * width,
                y * height,
                radius * width,
                0,
                Math.PI * 2,
            );
            ctx.fillStyle = `rgba(255, 255, 255, ${opacity})`;
            ctx.fill();
            ctx.closePath();
        }

        const gradient = ctx.createRadialGradient(
            0.5 * width,
            0.5 * height,
            0,
            0.5 * width,
            0.5 * height,
            width,
        );
        gradient.addColorStop(0, 'rgba(255, 255, 255, 0.2)');
        gradient.addColorStop(0.4, 'rgba(255, 255, 255, 0)');
        gradient.addColorStop(0.5, 'rgba(0, 0, 0, 0)');
        gradient.addColorStop(1, 'rgba(0, 0, 0, 0.3)');

        ctx.beginPath();
        ctx.fillStyle = gradient;
        ctx.fillRect(-20, -20, width + 20, height + 20);
        ctx.closePath();
    }

    render() {
        const {width, height} = this.state;
        return (
            <div
                ref={this.onWrapperRef}
                className={this.props.className}
            >
                <canvas
                    ref={this.onCanvasRef}
                    width={width}
                    height={height}
                    style={{
                        width: '100%',
                        height: '100%',
                    }}
                />
            </div>
        )
    }
}

export default ScreenBackground;
