import React from 'react';
import PropTypes from 'prop-types';
import './viewport.css';

class Viewport extends React.Component {
    static propTypes = {
        width: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        height: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
        children: PropTypes.node,
    }
    static defaultProps = {
        width: '100%',
        height: '100%',
    }

    render() {
        const {width, height} = this.props;
        return (
            <div className="viewport">
                <div
                    className="viewport__content"
                    style={{width, height}}
                >
                    {this.props.children}
                </div>
            </div>
        );
    }
}

export default Viewport;
