import React from 'react';
import PropTypes from 'prop-types';
import './button.css';

class Button extends React.Component {
    static propTypes = {
        text: PropTypes.string.isRequired,
        disabled: PropTypes.bool,
        onClick: PropTypes.func,
    }

    render() {
        const {text, disabled, onClick} = this.props;
        return (
            <button
                className="button"
                disabled={disabled}
                onClick={onClick}
            >{text}</button>
        );
    }
}

export default Button;
