import {combineReducers} from 'redux';
import account from './screens/Account/reducer';
import exchange from './screens/Exchange/reducer';

export default combineReducers({
    account,
    exchange,
});
