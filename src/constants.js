export const CURRENCY = {
    EUR: 'EUR',
    GBP: 'GBP',
    USD: 'USD',
};

export const CURRENCY_SYMBOL = {
    [CURRENCY.EUR]: '€',
    [CURRENCY.GBP]: '£',
    [CURRENCY.USD]: '$',
};
