import fetch from './mock';

export default {
    account: {
        balance: () => fetch('account/balance'),
    },
    exchange: {
        rates: () => fetch('exchange/rates'),
        commit: (pair, amount) => fetch('exchange/commit', {pair, amount}),
    },
};
