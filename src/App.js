import React, {Component} from 'react';
import {Provider} from 'react-redux'
import {createStore, applyMiddleware, compose} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from './reducer';
import rootSaga from './sagas';
import Viewport from './components/Viewport';
import ExchangeScreen from './screens/Exchange/ExchangeScreen';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);
sagaMiddleware.run(rootSaga);

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <Viewport
                    width={320}
                    height={480}
                >
                    <ExchangeScreen/>
                </Viewport>
            </Provider>
        );
    }
}

export default App;
