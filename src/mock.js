import {API_KEY} from './configuration';
import {CURRENCY} from './constants';

const {EUR, GBP, USD} = CURRENCY;

const sleep = delay => new Promise(resolve => setTimeout(resolve, delay));
const once = fn => {
    let fired = false;
    return (...args) => {
        if (fired) return;
        fired = true;
        return fn(...args);
    };
};

const data = {
    balance: {
        [EUR]: 1089,
        [GBP]: 94,
        [USD]: 220,
    },
    rates: {},
};

const currencies = [EUR, GBP, USD];
let pairs = [];
for (const selling of currencies) {
    for (const buying of currencies) {
        if (selling !== buying) {
            pairs.push(`${selling}${buying}`);
        }
    }
}
pairs = pairs.join(',');

const warn = once(
    () => alert('Something went wrong with fetching rates data. Possibly you have reached api calls limit or api token has been outdated, so stub data will be displayed. See console messages for details.')
);
const ratesStub =  {
    EURGBP: 0.9046,
    EURUSD: 1.16221,
    GBPEUR: 1.10546,
    GBPUSD: 1.28493,
    USDEUR: 0.86043,
    USDGBP: 0.778253
};

const mockedFetch = async (url, params) => {
    await sleep(100);

    switch (url) {
        case 'account/balance':
            return data.balance;
        case 'exchange/rates': {
            let result;
            try {
                const response = await fetch(`https://forex.1forge.com/1.0.1/quotes?pairs=${pairs}&api_key=${API_KEY}`);
                result = await response.json();

                if (result.error) throw new Error(result.message);

                data.rates = result.reduce((result, rate) => ({
                    ...result,
                    [rate.symbol]: rate.price,
                }), {});
            } catch (e) {
                console.error(e);
                warn();
                data.rates = ratesStub;
            }

            return data.rates;
        }
        case 'exchange/commit': {
            const {pair, amount} = params;
            const rate = data.rates[params.pair];
            const buyingAmount = rate * params.amount;

            const sellingCurrency = pair.slice(0, 3);
            const buyingCurrency = pair.slice(3);

            if (data.balance[sellingCurrency] - amount < 0) {
                throw new Error('Not enough money in account');
            }

            data.balance[sellingCurrency] -= amount;
            data.balance[buyingCurrency] += buyingAmount;

            return data.balance;
        }
        default:
            throw new Error(`Url "${url}" is not mocked`);
    }
};

export default async (...params) => {
    const response = await mockedFetch(...params);

    return JSON.parse(JSON.stringify(response));
};
