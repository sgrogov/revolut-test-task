import {all} from 'redux-saga/effects';
import accountSaga from './screens/Account/sagas';
import exchangeSaga from './screens/Exchange/sagas';

export default function* rootSaga() {
    yield all([
        accountSaga(),
        exchangeSaga(),
    ]);
};
